@echo off

set msbuild="C:\Program Files (x86)\MSBuild\14.0\Bin\MSBuild.exe"

%msbuild% .\src\FileSync.sln /p:Configuration=Debug /t:Clean;Rebuild /p:OutputPath=.\..\..\bin\Debug /m
FOR /F "tokens=*" %%G IN ('DIR /B /AD /S obj') DO RMDIR /S /Q "%%G"

%msbuild% .\src\FileSync.sln /p:Configuration=Release /t:Clean;Rebuild /p:OutputPath=.\..\..\bin\Release /m
FOR /F "tokens=*" %%G IN ('DIR /B /AD /S obj') DO RMDIR /S /Q "%%G"


pause