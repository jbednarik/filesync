namespace FileSync
{
    struct SourceTargetItem
    {
        public SourceTargetItem(string source, string target)
        {
            Source = source;
            Target = target;
        }

        public string Source { get; }
        public string Target { get; }
    }
}