using System;
using System.IO;
using System.Linq;

namespace FileSync
{
    /// <summary>
    ///     Instance of directory tied to physical directory on device. <seealso cref="DirectoryInfo" />
    /// </summary>
    class SystemDirectory : IDirectory
    {
        readonly DirectoryInfo directory;

        /// <summary>
        ///     Initializes a new instance of the <see cref="SystemDirectory" /> class.
        /// </summary>
        /// <param name="directory">The directory.</param>
        public SystemDirectory(DirectoryInfo directory)
        {
            this.directory = directory;
        }

        /// <summary>
        ///     Gets a value indicating whether the directory is exists.
        /// </summary>
        /// <value>
        ///     Returns <c>true</c> if exists; otherwise, <c>false</c>.
        /// </value>
        public bool Exists => directory.Exists;

        /// <summary>
        ///     Gets the full name.
        /// </summary>
        /// <value>
        ///     The full name.
        /// </value>
        public string FullName => directory.FullName;

        /// <summary>
        ///     Gets or sets the creation time UTC.
        /// </summary>
        /// <value>
        ///     The creation time UTC.
        /// </value>
        public DateTime CreationTimeUtc
        {
            get { return directory.CreationTimeUtc; }
            set { directory.CreationTimeUtc = value; }
        }

        /// <summary>
        ///     Gets or sets the last access time UTC.
        /// </summary>
        /// <value>
        ///     The last access time UTC.
        /// </value>
        public DateTime LastAccessTimeUtc
        {
            get { return directory.LastAccessTimeUtc; }
            set { directory.LastAccessTimeUtc = value; }
        }

        /// <summary>
        ///     Gets or sets the last write time UTC.
        /// </summary>
        /// <value>
        ///     The last write time UTC.
        /// </value>
        public DateTime LastWriteTimeUtc
        {
            get { return directory.LastWriteTimeUtc; }
            set { directory.LastWriteTimeUtc = value; }
        }

        /// <summary>
        ///     Gets or sets the attributes.
        /// </summary>
        /// <value>
        ///     The attributes.
        /// </value>
        public FileAttributes Attributes
        {
            get { return directory.Attributes; }
            set { directory.Attributes = value; }
        }

        /// <summary>
        ///     Gets the directories within this directory.
        /// </summary>
        /// <param name="fileFilter">The file filter.</param>
        /// <param name="topDirectoryOnly">The top directory only.</param>
        /// <returns></returns>
        public IDirectory[] GetDirectories(string fileFilter, SearchOption topDirectoryOnly)
        {
            return directory.GetDirectories(fileFilter, topDirectoryOnly).Select(x => new SystemDirectory(x)).Cast<IDirectory>().ToArray();
        }

        /// <summary>
        ///     Deletes the directory.
        /// </summary>
        /// <param name="recursive">if set to <c>true</c> deletes the content as well.</param>
        public void Delete(bool recursive)
        {
            directory.Delete(recursive);
        }

        /// <summary>
        ///     Creates the subdirectory.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        public IDirectory CreateSubdirectory(string path)
        {
            return new SystemDirectory(directory.CreateSubdirectory(path));
        }

        /// <summary>
        ///     Gets the files.
        /// </summary>
        /// <param name="filter">The name filter.</param>
        /// <param name="searchOption">The search option.</param>
        /// <returns></returns>
        public IFile[] GetFiles(string filter, SearchOption searchOption)
        {
            return directory.GetFiles(filter, searchOption).Select(info => new SystemFile(info)).Cast<IFile>().ToArray();
        }
    }
}