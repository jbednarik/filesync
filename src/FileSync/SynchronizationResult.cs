﻿using System;

namespace FileSync
{
    class SynchronizationResult
    {
        /// <summary>
        ///     Gets or sets the source.
        /// </summary>
        /// <value>
        ///     The source.
        /// </value>
        public string Source { get; set; }

        /// <summary>
        ///     Gets or sets the target.
        /// </summary>
        /// <value>
        ///     The target.
        /// </value>
        public string Target { get; set; }

        /// <summary>
        ///     Gets or sets the duration.
        /// </summary>
        /// <value>
        ///     The duration.
        /// </value>
        public TimeSpan Duration { get; set; }

        /// <summary>
        ///     Gets or sets the start.
        /// </summary>
        /// <value>
        ///     The start.
        /// </value>
        public DateTime Start { get; set; }

        /// <summary>
        ///     Gets or sets the finished.
        /// </summary>
        /// <value>
        ///     The finished.
        /// </value>
        public DateTime Finished { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether any error occurred.
        /// </summary>
        /// <value>
        ///     <c>true</c> if error occurred; otherwise, <c>false</c>.
        /// </value>
        public bool ErrorOccurred { get; set; }

        /// <summary>
        ///     Gets or sets the output.
        /// </summary>
        /// <value>
        ///     The output.
        /// </value>
        public string Output { get; set; }

        /// <summary>
        /// Gets or sets the average speed in B/s.
        /// </summary>
        /// <value>
        /// The average speed.
        /// </value>
        public double AverageSpeed { get; set; }
    }
}