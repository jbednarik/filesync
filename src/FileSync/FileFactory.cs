﻿using System.IO;

namespace FileSync
{
    class FileFactory : IFileFactory
    {
        public IFile Create(string path)
        {
            return new SystemFile(new FileInfo(path));
        }

        public string GetPath(string path1, string path2)
        {
            return Path.Combine(path1, path2.TrimStart('\\'));
        }
    }
}