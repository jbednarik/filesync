﻿using System;
using System.IO;

namespace FileSync
{
    interface IFile
    {
        string FullName { get; }
        long Length { get; }
        DateTime CreationTimeUtc { get; set; }
        DateTime LastAccessTimeUtc { get; set; }
        DateTime LastWriteTimeUtc { get; set; }
        FileAttributes Attributes { get; set; }
        void Delete();
        Stream OpenRead();
        void Refresh();
    }
}