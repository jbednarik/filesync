using System;
using System.Collections.Generic;
using System.Linq;

namespace FileSync
{
    public class FileSizeFormatProvider : IFormatProvider, ICustomFormatter
    {
        const decimal Step = 1024;

        public const string FileSizeFormat = "fs";

        /// <summary>
        ///     The default instance
        /// </summary>
        public static readonly FileSizeFormatProvider Instance = new FileSizeFormatProvider();

        static readonly Dictionary<decimal, string> Map = new Dictionary<decimal, string>
                                                          {
                                                              [0] = "B",
                                                              [1024] = "kB",
                                                              [1048576] = "MB",
                                                              [1073741824] = "GB",
                                                              [1073741824*Step] = "TB"
                                                          };

        static readonly Dictionary<decimal, string> MapReversed = Map.Reverse().ToDictionary(x => x.Key, x => x.Value);

        public string Format(string format, object arg, IFormatProvider formatProvider)
        {
            if (string.IsNullOrEmpty(format) || !format.StartsWith(FileSizeFormat))
                return Defaultformat(format, arg, formatProvider);

            if (arg is string)
                return Defaultformat(format, arg, formatProvider);

            decimal size;

            try
            {
                size = Convert.ToDecimal(arg, formatProvider);
            }
            catch (InvalidCastException)
            {
                return Defaultformat(format, arg, formatProvider);
            }

            string suffix = null;
            // ReSharper disable once AccessToModifiedClosure - no need since the cycle breaks
            foreach (var pair in MapReversed.Where(pair => size >= pair.Key))
            {
                suffix = pair.Value;

                if (pair.Key != 0)
                    size /= pair.Key;

                break;
            }
            suffix = suffix ?? Map[0];

            var precision = format.Substring(2);
            if (string.IsNullOrEmpty(precision)) precision = "2";
            return string.Format("{0:N" + precision + "}{1}", size, suffix);
        }

        public object GetFormat(Type formatType)
        {
            if (formatType == typeof (ICustomFormatter)) return this;
            return null;
        }

        static string Defaultformat(string format, object arg, IFormatProvider formatProvider)
        {
            var formattableArg = arg as IFormattable;
            return formattableArg?.ToString(format, formatProvider) ?? arg.ToString();
        }
    }
}