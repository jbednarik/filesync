﻿using System;

namespace FileSync
{
    class SyncDirectoryItem : IEquatable<SyncDirectoryItem>
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="SyncDirectoryItem" /> class.
        /// </summary>
        /// <param name="relativePath">The relative path.</param>
        public SyncDirectoryItem(string relativePath)
        {
            RelativePath = relativePath;
        }

        /// <summary>
        ///     Gets or sets the relative path.
        /// </summary>
        /// <value>
        ///     The relative path.
        /// </value>
        public string RelativePath { get; }

        /// <summary>
        ///     Gets or sets the directory.
        /// </summary>
        /// <value>
        ///     The directory.
        /// </value>
        public IDirectory Directory { get; set; }

        public bool Equals(SyncDirectoryItem other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(RelativePath, other.RelativePath);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((SyncDirectoryItem) obj);
        }

        public override int GetHashCode()
        {
            return RelativePath?.GetHashCode() ?? 0;
        }

        public static bool operator ==(SyncDirectoryItem left, SyncDirectoryItem right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(SyncDirectoryItem left, SyncDirectoryItem right)
        {
            return !Equals(left, right);
        }
    }
}