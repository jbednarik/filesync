﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace FileSync
{
    class FileSystemSynchronizer
    {
        const string FileFilter = "*.*";
        readonly IDirectoryFactory directoryFactory;

        readonly IFileFactory fileFactory;
        TextWriter logWriter;

        IDirectory sourceRoot;
        IDirectory targetRoot;
        readonly List<double> transferSpeed;

        /// <summary>
        ///     Initializes a new instance of the <see cref="FileSystemSynchronizer" /> class.
        /// </summary>
        /// <param name="fileFactory">The file factory.</param>
        /// <param name="directoryFactory">The directory factory.</param>
        /// <exception cref="System.ArgumentNullException">
        /// </exception>
        public FileSystemSynchronizer(IFileFactory fileFactory, IDirectoryFactory directoryFactory)
        {
            if (fileFactory == null) throw new ArgumentNullException(nameof(fileFactory));
            if (directoryFactory == null) throw new ArgumentNullException(nameof(directoryFactory));

            this.fileFactory = fileFactory;
            this.directoryFactory = directoryFactory;

            transferSpeed = new List<double>();
        }

        /// <summary>
        ///     Gets or sets the maximum retries in case recoverable error occures.
        /// </summary>
        /// <value>
        ///     The maximum retries.
        /// </value>
        public int MaxRetries { get; set; } = 5;

        /// <summary>
        ///     Gets or sets a value indicating whether error should be logged with callstack to output.
        /// </summary>
        /// <value>
        ///     <c>true</c> if log errors with stack; otherwise, <c>false</c>.
        /// </value>
        public bool LogErrorsWithStack { get; set; } = false;

        /// <summary>
        ///     Gets or sets the size of the buffer used during stream operations.
        /// </summary>
        /// <value>
        ///     The size of the buffer.
        /// </value>
        public int BufferSize { get; set; } = 65536;

        /// <summary>
        ///     Gets a value indicating whether an error occured durint synchronization.
        /// </summary>
        /// <value>
        ///     <c>true</c> if error occured; otherwise, <c>false</c>.
        /// </value>
        public bool ErrorOccured { get; private set; }

        /// <summary>
        ///     Synchronizes files and directories between <paramref name="source" /> and <paramref name="target" />.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="target">The target.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="source" /> is <see langword="null" />
        ///     or
        ///     <paramref name="target" /> is <see langword="null" />
        ///     .
        /// </exception>
        /// <exception cref="FileSynchronizationException">
        ///     <paramref name="target" /> directory does not exist
        ///     or
        ///     <paramref name="target" />directory does not exist.
        /// </exception>
        /// <exception cref="PathTooLongException">The caller does not have the required permission.</exception>
        /// <exception cref="FileSynchronizationException">
        ///     <paramref name="target" /> directory does not exist
        ///     or
        ///     <paramref name="target" />directory does not exist.
        /// </exception>
        public async Task<SynchronizationResult> SynchronizeAsync(IDirectory source, IDirectory target)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            if (target == null) throw new ArgumentNullException(nameof(target));

            if (!source.Exists)
                throw new FileSynchronizationException($"Source {source.FullName} path does not exist.");
            if (!target.Exists)
                throw new FileSynchronizationException($"Target {target.FullName} path does not exist.");

            transferSpeed.Clear();

            var output = new StringBuilder();
            logWriter = new StringWriter(output);

            if (Environment.UserInteractive)
                logWriter = new CompositeTextWriter(new StringWriter(output), Console.Out);

            sourceRoot = source;
            targetRoot = target;

            var start = DateTime.UtcNow;

            await Synchronize(source, target)
                .ConfigureAwait(false);

            var finished = DateTime.UtcNow;

            return new SynchronizationResult
            {
                Duration = finished - start,
                Start = start,
                Finished = finished,
                ErrorOccurred = ErrorOccured,
                AverageSpeed = transferSpeed.Count == 0 ? 0 : transferSpeed.Average(),
                Source = source.FullName,
                Target = target.FullName,
                Output = output.ToString()
            };
        }

        async Task Synchronize(IDirectory source, IDirectory target)
        {
            logWriter.WriteLine($"{MakePathRelative(source.FullName, sourceRoot.FullName)} -> {MakePathRelative(target.FullName, targetRoot.FullName)}");

            await Files(source, target)
                .ConfigureAwait(false);

            await Directories(source, target)
                .ConfigureAwait(false);
        }

        async Task Directories(IDirectory source, IDirectory target)
        {
            var targetDirectories = target.GetDirectories(FileFilter, SearchOption.TopDirectoryOnly)
                                          .Select(x => new SyncDirectoryItem(MakePathRelative(x.FullName, targetRoot.FullName))
                                          {
                                              Directory = x
                                          }).ToArray();
            var sourceDirectories = source.GetDirectories(FileFilter, SearchOption.TopDirectoryOnly)
                                          .Select(x => new SyncDirectoryItem(MakePathRelative(x.FullName, sourceRoot.FullName))
                                          {
                                              Directory = x
                                          }).ToArray();

            var sourceLookup = sourceDirectories.ToLookup(x => x.RelativePath);
            var targetLookup = targetDirectories.ToLookup(x => x.RelativePath);


            var query = from s in sourceDirectories.AsEnumerable().Concat(targetDirectories.AsEnumerable())
                        group s by s.RelativePath
                        into g
                        select new
                        {
                            RelativePath = g.Key,
                            Source = sourceLookup[g.Key].FirstOrDefault(),
                            Target = targetLookup[g.Key].FirstOrDefault()
                        };


            foreach (var item in query)
            {
                // directory no longer exists in source... delete in target
                if (item.Source == null && item.Target != null)
                {
                    logWriter.WriteLine($"[not found] -> {item.RelativePath}");

                    Delete(item.Target, directoryItem => directoryItem.Directory.Delete(true));

                    continue;
                }

                // directory is new in source... create in target
                // if fails continue with next one (no retries)
                if (item.Source != null && item.Target == null)
                {
                    logWriter.WriteLine($"{item.RelativePath} (new) -> {item.RelativePath}");

                    // create subdirectory

                    IDirectory dir;

                    logWriter.Write($"  creating...");
                    try
                    {
                        dir = targetRoot.CreateSubdirectory(item.RelativePath.TrimStart('\\'));

                        dir.CreationTimeUtc = item.Source.Directory.CreationTimeUtc;
                        dir.LastAccessTimeUtc = item.Source.Directory.LastAccessTimeUtc;
                        dir.LastWriteTimeUtc = item.Source.Directory.LastWriteTimeUtc;
                        dir.Attributes = item.Source.Directory.Attributes;
                    }
                    catch (UnauthorizedAccessException)
                    {
                        logWriter.WriteLine($"failed -> unauthorized access.");
                        continue;
                    }
                    catch (IOException)
                    {
                        logWriter.WriteLine($"failed -> generic IO error.");
                        continue;
                    }
                    catch (SecurityException)
                    {
                        logWriter.WriteLine($"failed -> security error.");
                        continue;
                    }

                    await Files(item.Source.Directory, dir)
                        .ConfigureAwait(false);
                    await Directories(item.Source.Directory, dir)
                        .ConfigureAwait(false);

                    continue;
                }

                // directory exists in source and in target... continue recursively
                if (item.Source != null && item.Target != null)
                {
                    logWriter.WriteLine(
                        $"{MakePathRelative(item.Source.Directory.FullName, sourceRoot.FullName)} -> {MakePathRelative(item.Target.Directory.FullName, targetRoot.FullName)}");

                    await Files(item.Source.Directory, item.Target.Directory)
                        .ConfigureAwait(false);
                    await Directories(item.Source.Directory, item.Target.Directory)
                        .ConfigureAwait(false);

                    continue;
                }

                Debug.Assert(false, "Source and/or target are not properly initialized");
            }
        }

        async Task Files(IDirectory source, IDirectory target)
        {
            var targetFiles = target.GetFiles(FileFilter, SearchOption.TopDirectoryOnly)
                                    .Select(x => new SyncFileItem(MakePathRelative(x.FullName, targetRoot.FullName))
                                    {
                                        File = x
                                    }).ToArray();
            var sourceFiles = source.GetFiles(FileFilter, SearchOption.TopDirectoryOnly)
                                    .Select(x => new SyncFileItem(MakePathRelative(x.FullName, sourceRoot.FullName))
                                    {
                                        File = x
                                    }).ToArray();
            var sourceLookup = sourceFiles.ToLookup(x => x.RelativePath);
            var targetLookup = targetFiles.ToLookup(x => x.RelativePath);

            var query = from s in sourceFiles.AsEnumerable().Concat(targetFiles.AsEnumerable())
                        group s by s.RelativePath
                        into g
                        select new
                        {
                            RelativePath = g.Key,
                            Source = sourceLookup[g.Key].FirstOrDefault(),
                            Target = targetLookup[g.Key].FirstOrDefault()
                        };

            foreach (var item in query)
            {
                if (item.Source == null && item.Target != null)
                {
                    logWriter.WriteLine($"[not found] -> {item.RelativePath}");
                    Delete(item.Target, fileItem => fileItem.File.Delete());
                    continue;
                }

                if (item.Source != null && item.Target == null)
                {
                    logWriter.WriteLine($"{item.RelativePath} (new) -> {item.RelativePath}");
                    await Upload(item.Source, null)
                        .ConfigureAwait(false);
                    continue;
                }

                if (item.Source != null && item.Target != null)
                {
                    if (HasChanged(item.Source, item.Target))
                    {
                        logWriter.WriteLine($"{item.RelativePath} (changed) -> {item.RelativePath}");
                        await Upload(item.Source, item.Target)
                            .ConfigureAwait(false);
                    }
                    else
                    {
                        logWriter.WriteLine($"{item.RelativePath} -> {item.RelativePath}");
                    }
                }
            }
        }

        void Delete<T>(T item, Action<T> delete)
        {
            var sw = Stopwatch.StartNew();
            var retriesLeft = MaxRetries;
            while (retriesLeft-- > 0)
            {
                logWriter.Write($"  deleting...");

                try
                {
                    delete(item);
                    sw.Stop();
                    var elapsed = sw.Elapsed;
                    logWriter.WriteLine($"completed in {elapsed}.");
                    break;
                }
                catch (UnauthorizedAccessException ex)
                {
                    logWriter.WriteLine("failed - unauthorized access.");
                    if (LogErrorsWithStack)
                        logWriter.WriteLine(ex.ToString());
                    ErrorOccured = true;
                    break;
                }
                catch (FileNotFoundException ex)
                {
                    logWriter.WriteLine("failed - file was not found.");
                    if (LogErrorsWithStack)
                        logWriter.WriteLine(ex.ToString());
                    ErrorOccured = true;
                    break;
                }
                catch (DirectoryNotFoundException ex)
                {
                    logWriter.WriteLine("failed - directory was not found.");
                    if (LogErrorsWithStack)
                        logWriter.WriteLine(ex.ToString());
                    ErrorOccured = true;
                    break;
                }
                catch (SecurityException ex)
                {
                    logWriter.WriteLine($"failed - {ex.Message}");
                    if (LogErrorsWithStack)
                        logWriter.WriteLine(ex.ToString());
                    ErrorOccured = true;
                    break;
                }
                catch (IOException ex)
                {
                    logWriter.WriteLine($" failed. Retrying ({retriesLeft + 1 - MaxRetries}/{MaxRetries})");
                    if (LogErrorsWithStack)
                        logWriter.WriteLine(ex.ToString());
                    ErrorOccured = true;
                }
            }
            sw.Stop();
        }

        async Task Upload(SyncFileItem source, SyncFileItem target)
        {
            if (target == null)
            {
                var path = fileFactory.GetPath(targetRoot.FullName, source.RelativePath);
                target = new SyncFileItem(source.RelativePath)
                {
                    File = fileFactory.Create(path)
                };
            }

            var sw = Stopwatch.StartNew();
            var retriesLeft = MaxRetries;
            while (retriesLeft-- > 0)
            {
                logWriter.Write($"  uploading ...");

                try
                {
                    using (var sourceStream = source.File.OpenRead())
                    using (var targetStream = new FileStream(target.File.FullName, FileMode.OpenOrCreate, FileAccess.Write))
                    {
                        targetStream.SetLength(0);

                        // TODO: is custom implementation using buffer array faster then this.. check implementation in bcl
                        await sourceStream.CopyToAsync(targetStream, BufferSize)
                                          .ConfigureAwait(false);
                    }

                    sw.Stop();
                    var elapsed = sw.Elapsed;

                    transferSpeed.Add(CalculateTransferSpeed(source.File.Length, sw.Elapsed));

                    var friendlySpeed = string.Format(FileSizeFormatProvider.Instance, "{0:fs}/s", source.File.Length / sw.Elapsed.TotalSeconds);

                    logWriter.WriteLine($"completed in {elapsed} [{friendlySpeed}].");

                    target.File.Refresh();
                    target.File.CreationTimeUtc = source.File.CreationTimeUtc;
                    target.File.LastAccessTimeUtc = source.File.LastAccessTimeUtc;
                    target.File.LastWriteTimeUtc = source.File.LastWriteTimeUtc;
                    target.File.Attributes = source.File.Attributes;
                    break;
                }
                catch (UnauthorizedAccessException ex)
                {
                    logWriter.WriteLine("failed - unauthorized access.");
                    if (LogErrorsWithStack)
                        logWriter.WriteLine(ex.ToString());
                    ErrorOccured = true;
                    break;
                }
                catch (FileNotFoundException ex)
                {
                    logWriter.WriteLine("failed - file was not found.");
                    if (LogErrorsWithStack)
                        logWriter.WriteLine(ex.ToString());
                    ErrorOccured = true;
                    break;
                }
                catch (SecurityException ex)
                {
                    logWriter.WriteLine($"failed - {ex.Message}");
                    if (LogErrorsWithStack)
                        logWriter.WriteLine(ex.ToString());
                    ErrorOccured = true;
                    break;
                }
                catch (IOException ex)
                {
                    logWriter.WriteLine($" failed - {ex.Message}. Retrying ({MaxRetries - retriesLeft}/{MaxRetries})");
                    if (LogErrorsWithStack)
                        logWriter.WriteLine(ex.ToString());
                    ErrorOccured = true;
                }
            }
        }

        double CalculateTransferSpeed(long length, TimeSpan duration)
        {
            return length / duration.TotalSeconds;
        }

        bool HasChanged(SyncFileItem source, SyncFileItem target)
        {
            return source.File.Length != target.File.Length ||
                   source.File.LastWriteTimeUtc != target.File.LastWriteTimeUtc;
        }

        /// <summary>
        ///     Makes <paramref name="filePath" /> relative regarding to <paramref name="rootPath" />.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="rootPath">The root path.</param>
        /// <returns></returns>
        string MakePathRelative(string filePath, string rootPath)
        {
            rootPath = rootPath.MakeSureEndsWith("\\");
            if (directoryFactory.DirectoryExists(filePath))
                filePath = filePath.MakeSureEndsWith("\\");

            var fileUri = new Uri(filePath);
            var rootUri = new Uri(rootPath);
            var relative = rootUri.MakeRelativeUri(fileUri).ToString();
            relative = HttpUtility.UrlDecode(relative);
            relative = relative.Replace("/", "\\");
            relative = relative.MakeSureStartsWith("\\");
            return relative;
        }

        class CompositeTextWriter : TextWriter
        {
            readonly TextWriter[] writers;

            public CompositeTextWriter(params TextWriter[] writers)
            {
                this.writers = writers;
            }

            public override void WriteLine()
            {
                foreach (var textWriter in writers)
                {
                    textWriter.WriteLine();
                }
            }

            public override void WriteLine(string value)
            {
                foreach (var textWriter in writers)
                {
                    textWriter.WriteLine(value);
                }
            }

            public override Encoding Encoding => writers[0].Encoding;

            public override void Flush()
            {
                foreach (var textWriter in writers)
                {
                    textWriter.Flush();
                }
            }
        }
    }
}