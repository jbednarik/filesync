﻿namespace FileSync
{
    interface IFileFactory
    {
        IFile Create(string path);
        string GetPath(string path1, string path2);
    }
}