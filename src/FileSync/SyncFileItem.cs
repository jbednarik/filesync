using System;

namespace FileSync
{
    class SyncFileItem : IEquatable<SyncFileItem>
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="SyncFileItem" /> class.
        /// </summary>
        /// <param name="relativePath">The relative path.</param>
        public SyncFileItem(string relativePath)
        {
            RelativePath = relativePath;
        }

        /// <summary>
        ///     Gets or sets the relative path.
        /// </summary>
        /// <value>
        ///     The relative path.
        /// </value>
        public string RelativePath { get; }

        /// <summary>
        ///     Gets or sets the file.
        /// </summary>
        /// <value>
        ///     The file.
        /// </value>
        public IFile File { get; set; }

        public bool Equals(SyncFileItem other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(RelativePath, other.RelativePath);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((SyncFileItem) obj);
        }

        public override int GetHashCode()
        {
            return RelativePath?.GetHashCode() ?? 0;
        }

        public static bool operator ==(SyncFileItem left, SyncFileItem right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(SyncFileItem left, SyncFileItem right)
        {
            return !Equals(left, right);
        }
    }
}