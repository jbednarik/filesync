namespace FileSync
{
    public interface IFileSyncConfiguration
    {
        int MaxRetries { get; }
        int BufferSize { get; }
        bool LogErrorsWithStack { get; }
        string ReportSenderMailAddress { get; }
        string ReportRecipientMailAddresses { get; }
        string ReportMailSubjectPrefix { get; }
        bool UseSecureSmtp { get; }
    }
}