﻿using System.IO;

namespace FileSync
{
    class DirectoryFactory : IDirectoryFactory
    {
        public IDirectory Create(string path)
        {
            return new SystemDirectory(new DirectoryInfo(path));
        }

        public bool DirectoryExists(string path)
        {
            return Directory.Exists(path);
        }
    }
}