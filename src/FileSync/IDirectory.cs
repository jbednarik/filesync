﻿using System;
using System.IO;

namespace FileSync
{
    interface IDirectory
    {
        bool Exists { get; }
        string FullName { get; }
        DateTime CreationTimeUtc { get; set; }
        DateTime LastAccessTimeUtc { get; set; }
        DateTime LastWriteTimeUtc { get; set; }
        FileAttributes Attributes { get; set; }
        IDirectory[] GetDirectories(string filter, SearchOption searchOption);
        void Delete(bool recursive);
        IDirectory CreateSubdirectory(string path);
        IFile[] GetFiles(string filter, SearchOption searchOption);
    }
}