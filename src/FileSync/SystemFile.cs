﻿using System;
using System.IO;

namespace FileSync
{
    /// <summary>
    ///     Instance of file tied to physical file on device. <seealso cref="FileInfo" />
    /// </summary>
    class SystemFile : IFile
    {
        readonly FileInfo file;

        /// <summary>
        ///     Initializes a new instance of the <see cref="SystemFile" /> class.
        /// </summary>
        /// <param name="file">The file.</param>
        public SystemFile(FileInfo file)
        {
            this.file = file;
        }

        /// <summary>
        ///     Gets the full name.
        /// </summary>
        /// <value>
        ///     The full name.
        /// </value>
        public string FullName => file.FullName;

        /// <summary>
        ///     Gets the length.
        /// </summary>
        /// <value>
        ///     The length.
        /// </value>
        public long Length => file.Length;

        /// <summary>
        ///     Gets or sets the creation time UTC.
        /// </summary>
        /// <value>
        ///     The creation time UTC.
        /// </value>
        public DateTime CreationTimeUtc
        {
            get { return file.CreationTimeUtc; }
            set { file.CreationTimeUtc = value; }
        }

        /// <summary>
        ///     Gets or sets the last access time UTC.
        /// </summary>
        /// <value>
        ///     The last access time UTC.
        /// </value>
        public DateTime LastAccessTimeUtc
        {
            get { return file.LastAccessTimeUtc; }
            set { file.LastAccessTimeUtc = value; }
        }

        /// <summary>
        ///     Gets or sets the last write time UTC.
        /// </summary>
        /// <value>
        ///     The last write time UTC.
        /// </value>
        public DateTime LastWriteTimeUtc
        {
            get { return file.LastWriteTimeUtc; }
            set { file.LastWriteTimeUtc = value; }
        }

        /// <summary>
        ///     Gets or sets the attributes.
        /// </summary>
        /// <value>
        ///     The attributes.
        /// </value>
        public FileAttributes Attributes
        {
            get { return file.Attributes; }
            set { file.Attributes = value; }
        }

        /// <summary>
        ///     Deletes the file.
        /// </summary>
        public void Delete()
        {
            file.Delete();
        }

        /// <summary>
        ///     Opens the file for reading.
        /// </summary>
        /// <returns></returns>
        public Stream OpenRead()
        {
            return file.OpenRead();
        }

        /// <summary>
        ///     Refreshes the underlying data.
        /// </summary>
        public void Refresh()
        {
            file.Refresh();
        }
    }
}