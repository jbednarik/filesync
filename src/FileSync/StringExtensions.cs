﻿namespace FileSync
{
    static class StringExtensions
    {
        /// <summary>
        ///     Makes the sure the <paramref name="source" /> starts with <paramref name="value" />.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static string MakeSureStartsWith(this string source, string value)
        {
            if (!source.StartsWith(value))
                return value + source;
            return source;
        }

        /// <summary>
        ///     Makes the sure the <paramref name="source" /> ends with <paramref name="value" />.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static string MakeSureEndsWith(this string source, string value)
        {
            if (!source.EndsWith(value))
                return source + value;
            return source;
        }
    }
}