using System;
using System.Runtime.Serialization;

namespace FileSync
{
    [Serializable]
    public class FileSynchronizationException : Exception
    {
        public FileSynchronizationException()
        {
        }

        public FileSynchronizationException(string message)
            : base(message)
        {
        }

        public FileSynchronizationException(string message, Exception inner)
            : base(message, inner)
        {
        }


        protected FileSynchronizationException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}