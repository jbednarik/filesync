﻿using System.Configuration;

namespace FileSync
{
    class AppConfigFileSyncSetting : ConfigurationSection, IFileSyncConfiguration
    {
        [ConfigurationProperty("MaxRetries", DefaultValue = 5)]
        public int MaxRetries
        {
            get { return (int) this["MaxRetries"]; }
            set { this["MaxRetries"] = value; }
        }

        [ConfigurationProperty("BufferSize", DefaultValue = 64)]
        public int BufferSize
        {
            get { return (int) this["BufferSize"]; }
            set { this["BufferSize"] = value; }
        }

        [ConfigurationProperty("LogErrorsWithStack", DefaultValue = false)]
        public bool LogErrorsWithStack
        {
            get { return (bool) this["LogErrorsWithStack"]; }
            set { this["LogErrorsWithStack"] = value; }
        }

        [ConfigurationProperty("ReportSenderMailAddress", DefaultValue = "noreply@hostname.local")]
        public string ReportSenderMailAddress
        {
            get { return (string) this["ReportSenderMailAddress"]; }
            set { this["ReportSenderMailAddress"] = value; }
        }

        [ConfigurationProperty("ReportRecipientMailAddresses")]
        public string ReportRecipientMailAddresses
        {
            get { return (string) this["ReportRecipientMailAddresses"]; }
            set { this["ReportRecipientMailAddresses"] = value; }
        }

        [ConfigurationProperty("ReportMailSubject", DefaultValue = "Synchronization report")]
        public string ReportMailSubjectPrefix
        {
            get { return (string) this["ReportMailSubject"]; }
            set { this["ReportMailSubject"] = value; }
        }

        [ConfigurationProperty("UseSecureSmtp", DefaultValue = true)]
        public bool UseSecureSmtp
        {
            get { return (bool) this["UseSecureSmtp"]; }
            set { this["UseSecureSmtp"] = value; }
        }

        /// <summary>
        ///     Loads the configuration from application config file.
        /// </summary>
        /// <returns></returns>
        public static IFileSyncConfiguration LoadFromAppConfig()
        {
            IFileSyncConfiguration fileSyncSetting = ConfigurationManager.GetSection("FileSyncSettings") as AppConfigFileSyncSetting;
            return fileSyncSetting;
        }
    }
}