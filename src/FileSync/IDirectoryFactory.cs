﻿namespace FileSync
{
    interface IDirectoryFactory
    {
        IDirectory Create(string path);
        bool DirectoryExists(string path);
    }
}