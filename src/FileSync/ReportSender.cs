﻿using System;
using System.Net.Mail;
using System.Text;

namespace FileSync
{
    class ReportSender
    {
        readonly IFileSyncConfiguration config;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ReportSender" /> class.
        /// </summary>
        /// <param name="config">The configuration.</param>
        /// <exception cref="System.ArgumentNullException"></exception>
        public ReportSender(IFileSyncConfiguration config)
        {
            if (config == null) throw new ArgumentNullException(nameof(config));

            this.config = config;
        }

        /// <summary>
        ///     Sends the specified result.
        /// </summary>
        /// <param name="result">The result.</param>
        /// <exception cref="System.ArgumentNullException"></exception>
        public void Send(SynchronizationResult result)
        {
            if (result == null) throw new ArgumentNullException(nameof(result));

            var msg = CreateMailMessage();
            msg.Body = CreateBody(result);

            using (var client = CreateSmtpClient())
            {
                client.Send(msg);
            }
        }


        /// <summary>
        ///     Sends the specified exeption.
        /// </summary>
        /// <param name="ex">The exception.</param>
        public void Send(Exception ex)
        {
            var msg = CreateMailMessage();
            msg.Body = CreateBody(ex);

            using (var client = CreateSmtpClient())
            {
                client.Send(msg);
            }
        }

        SmtpClient CreateSmtpClient()
        {
            return new SmtpClient
            {
                EnableSsl = config.UseSecureSmtp
            };
        }

        string CreateBody(Exception result)
        {
            var msgBodyBuilder = new StringBuilder();
            msgBodyBuilder.AppendLine("Synchronization report");
            msgBodyBuilder.AppendLine();
            msgBodyBuilder.AppendLine($"Start: {DateTime.UtcNow}");

            msgBodyBuilder.AppendLine("Unexpected error occured during synchronization.");

            msgBodyBuilder.AppendLine(config.LogErrorsWithStack ? result.ToString() : result.Message);

            return msgBodyBuilder.ToString();
        }

        string CreateBody(SynchronizationResult result)
        {
            var msgBodyBuilder = new StringBuilder();
            msgBodyBuilder.AppendLine("Synchronization report");
            msgBodyBuilder.AppendLine();
            msgBodyBuilder.AppendLine($"{result.Source} -> {result.Target}");
            msgBodyBuilder.AppendLine();
            msgBodyBuilder.AppendLine($"Started  : {result.Start}");
            msgBodyBuilder.AppendLine($"Finished : {result.Finished}");
            msgBodyBuilder.AppendLine($"Duration : {result.Duration}");
            msgBodyBuilder.AppendLine($"Avg Speed: {(result.AverageSpeed / 1024 / 1024):F2}MB/s");
            msgBodyBuilder.AppendLine($"Clean run: {!result.ErrorOccurred}");
            if (result.ErrorOccurred)
                msgBodyBuilder.AppendLine(" there was at least one error during synchronization. See the log below.");
            msgBodyBuilder.AppendLine();
            msgBodyBuilder.AppendLine();
            msgBodyBuilder.AppendLine("Synchronization log:");
            msgBodyBuilder.AppendLine();
            msgBodyBuilder.AppendLine(result.Output);

            return msgBodyBuilder.ToString();
        }

        /// <summary>
        ///     Creates the mail with sender, recipients and sujbect set.
        /// </summary>
        /// <returns></returns>
        MailMessage CreateMailMessage()
        {
            var msg = new MailMessage();
            msg.Sender = new MailAddress(config.ReportSenderMailAddress);
            foreach (var recipient in config.ReportRecipientMailAddresses.Split(','))
            {
                msg.To.Add(recipient);
            }
            msg.Subject = config.ReportMailSubjectPrefix;
            return msg;
        }
    }
}