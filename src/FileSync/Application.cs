﻿using System;
using System.Collections.Generic;
using System.IO;

namespace FileSync
{
    class Application
    {
        readonly IFileSyncConfiguration config;
        readonly IDirectoryFactory directoryFactory;
        readonly IFileFactory fileFactory;
        readonly Func<ReportSender> getReportSender;
        readonly Func<IEnumerable<SourceTargetItem>> getSourceWithTargets;
        readonly Func<FileSystemSynchronizer> getSynchronizer;
        readonly TextWriter output;

        /// <summary>
        ///     Initializes a new instance of the <see cref="Application" /> class.
        /// </summary>
        /// <param name="getSynchronizer">The get synchronizer.</param>
        /// <param name="getReportSender">The get report sender.</param>
        /// <param name="getSourceWithTargets">The get source with targets.</param>
        /// <param name="directoryFactory">the directory factory.</param>
        /// <param name="output">The output.</param>
        /// <param name="fileFactory">The file factory.</param>
        /// <param name="config">The configuration.</param>
        /// <exception cref="System.ArgumentNullException">
        /// </exception>
        public Application(
            Func<FileSystemSynchronizer> getSynchronizer,
            Func<ReportSender> getReportSender,
            Func<IEnumerable<SourceTargetItem>> getSourceWithTargets,
            IFileFactory fileFactory,
            IDirectoryFactory directoryFactory,
            TextWriter output,
            IFileSyncConfiguration config)
        {
            if (getSynchronizer == null) throw new ArgumentNullException(nameof(getSynchronizer));
            if (getReportSender == null) throw new ArgumentNullException(nameof(getReportSender));
            if (getSourceWithTargets == null) throw new ArgumentNullException(nameof(getSourceWithTargets));
            if (fileFactory == null) throw new ArgumentNullException(nameof(fileFactory));
            if (directoryFactory == null) throw new ArgumentNullException(nameof(directoryFactory));
            if (output == null) throw new ArgumentNullException(nameof(output));

            this.getReportSender = getReportSender;
            this.getSourceWithTargets = getSourceWithTargets;
            this.fileFactory = fileFactory;
            this.directoryFactory = directoryFactory;
            this.output = output;
            this.config = config;
            this.getSynchronizer = getSynchronizer;
        }

        public void Run()
        {
            foreach (var sourceWithTarget in getSourceWithTargets())
            {
                var synchronizer = getSynchronizer();

                Exception exception = null;
                SynchronizationResult result = null;

                try
                {
                    var sourceDir = directoryFactory.Create(sourceWithTarget.Source);
                    var targetDir = directoryFactory.Create(sourceWithTarget.Target);

                    result = synchronizer.SynchronizeAsync(sourceDir, targetDir).Result;
                    output.WriteLine(result.Output);
                }
                catch (Exception ex)
                {
                    exception = ex;
                }

                var sender = getReportSender();
                try
                {
                    if (exception != null)
                        sender.Send(exception);
                    else
                        sender.Send(result);

                    output.WriteLine("Report sent.");
                }
                catch (Exception ex)
                {
                    output.WriteLine("Error during sending report to smtp server.");
                    output.WriteLine(config.LogErrorsWithStack ? ex.ToString() : ex.Message);
                    Environment.Exit(-2);
                }
            }
        }
    }
}