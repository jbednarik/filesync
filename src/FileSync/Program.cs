﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FileSync
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("File synchronizer console utility. \u00a9 2015 JBe.");

            var config = AppConfigFileSyncSetting.LoadFromAppConfig();
            if (config == null)
            {
                Console.WriteLine("Unable to load settings from appconfig.");
                Environment.Exit(-1);
            }

            // Initialize composition root
            // TODO: Refactor to use proper composition root
            Func<IEnumerable<SourceTargetItem>> getSourceTargetItems = () => ParseArgs(args);
            var fileFactory = new FileFactory();
            var directoryFactory = new DirectoryFactory();
            Func<FileSystemSynchronizer> getSynchronizer = () => new FileSystemSynchronizer(fileFactory, directoryFactory);
            Func<ReportSender> getReportSender = () => new ReportSender(config);

            // execute application
            // TODO: too many dependencies? Try to refactor.
            var app = new Application(getSynchronizer, getReportSender, getSourceTargetItems, fileFactory, directoryFactory, Console.Out, config);
            app.Run();
        }

        static IEnumerable<SourceTargetItem> ParseArgs(string[] args)
        {
            string[] arr = string.Join(" ", args).Split(' ');
            if (arr.Length == 0)
            {
                Console.WriteLine("No input arguments.");
                PrintHelp();
                Environment.Exit(-3);
            }

            if (arr.Length % 2 != 0)
            {
                Console.WriteLine("Input arguments are not in pairs.");
                PrintHelp();
                Environment.Exit(-3);
            }

            var byTwo = arr.Select((s, i) => new { Value = s, Index = i })
                            .GroupBy(arg => arg.Index / 2, arg => arg.Value);

            foreach (var grouping in byTwo)
            {
                var items = grouping.ToArray();
                yield return new SourceTargetItem(items[0], items[1]);
            }
        }

        static void PrintHelp()
        {
            Console.WriteLine("Invalid program arguments.");
            Console.WriteLine("Arguments must be pairs of valid paths. First in pair represents source, the seconds represents target.");
            Console.WriteLine("Both endpoints must exist.");
            Console.WriteLine("Example:");
            Console.WriteLine(@"FileSync.exe ""C:\directory1\directory2\"" ""d:\backup\backup1\""");
        }
    }
}