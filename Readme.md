﻿# FileSync

A command-line application which synchronizes files in source directory to target 
directory recursively. User can specify multiple source and target pairs. The paths 
can be local paths or UNC network paths.

A report is generated with list of all synchronized files and apropriate actions taken
* changed - if source file changed;
* new - if there is a new file in source
* delete - if file/directory no longer exists in source

### Detecting changes
Changes in file are detected by last write access and size of the files. 
If target file is changed more recently then source it will be overwritten anyway.

### Report
For each specified source and target definition a report is generated and will be send 
using SMTP server to the recipient specified in app.config.


## Installation

Application does not need any installation. 


## Usage

Application must have defined at least two input arguments with valid path
to source and target directory. Both paths must be valid local or UNC paths.
The source & target definitions can be specified multiple times.

User must specify standard .net config mailsettings/smtp config section.
Application also must specify following configuration section type:
`type="FileSync.AppConfigFileSyncSetting, FileSync"`

with following properties:
<pre>
MaxRetries="5"
BufferSize="65536"
LogErrorsWithStack="true"
ReportSenderMailAddress="noreply@localhost.none"
ReportRecipientMailAddresses="recipient@emailaddress.com"
ReportMailSubject="Synchronization Report"
UseSecureSmtp="true"
</pre>

* MaxRetries - In case of IO exception how many retries must be executed until the file is skipped.
* BufferSize - Buffer size used to copy file from source to target.
* LogErrorsWithStack - True if errors should contain callstack
* ReportSenderMailAddress - Sender email address
* ReportRecipientMailAddresses - Address of the recipient of the report
* ReportMailSubject - Subject of the email
* UseSecureSmtp - True to use secure smtp connection

Test environment:
Windows 10 Pro N 
Last cumulative update KB3081448

Tested smtp server:
smtp.gmail.com with ssl connection

### Usage Examples:
<pre>
FileSync.exe "c:\source 1\" "d:\backup\source 1\"
FileSync.exe "c:\source 1\" "d:\backup\source 1\" "c:\source 2\" "\\backup\source 2\"
</pre>


## History

2015/09/07 - Initial version


## Credits

Jaroslav Bednarik


## License

The MIT License (MIT)

Copyright (c) 2015 Jaroslav Bednarik

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.